# Assignment_02


## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|game process|15%|Y|
|basic rules|20%|Y|
|Jucify mechanism|15%|Y|
|animation|5%|Y|
|particle system|5%|Y|
|sound effect|5%|Y|
|UI|5%|Y|
|Leaderboard|5%|Y|
|littleHelper|5%|Y|



# 作品網址：[ https://106062338.gitlab.io/Assignment_02 ]

# Components Description : 
1. [start menu => game view => game over => quit or play again] : [在首頁可以看到leaderboard及輸入username(沒輸入就不能開始)，play_again會進入首頁]
2. [basic rules] : [player只能左右動，可以被打到五次；enemy會往下掉的同時左右動，被打到一次enemy就會掛；地圖往下掉]
3. [jucify mechanism] : [level:總共4個等級，利用分數當判斷標準，越到後面敵人會變多，子彈會變快，敵人招式也會變機車。skill:連續打到5次敵人而且不被敵人打到就可以使用5次大絕招]
4. [animation] : [player是隻草尼瑪，左右動會有動畫，]
5. [particle system] : [player及enemy的子彈打中就會有particle效果]
6. [sound effects] : [包括bgm，enemyshoot的音效，player shoot的音效，玩家死掉的音效]
7. [UI] : [玩家有5條命；在skill==100%的時候可以使用絕招；音量條在右上，往上變小聲往下變大聲；暫停按p，resume也按p。]
8. [leaderboard] : [在首頁，顯示前五高的玩家及分數。]

# Bonus:
1. [helper] : [紅色馬桶會在第二關開始出現，吃到衛生紙可以加血並發動絕招；但如果射擊衛生紙就會扣血] 




