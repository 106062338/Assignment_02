var gameOverState = {
    create: function()
    {
        ////animation
        this.alpaca = game.add.sprite(game.width/7*5,game.height/3,'alpaca');
        this.alpaca.anchor.setTo(0.5, 0,5);
        game.add.tween(this.alpaca).to({y:game.height/3-10}, 300).to({y:game.height/3}, 300).loop().start();

        this.wing = game.add.sprite(game.width/7*5+10, game.height*2/5+20, 'wing');
        this.wing.anchor.setTo(0.5, 0.5);
        this.wing.animations.add('fly', [0,1], 4, true);
        console.log(this.wing);
        this.wing.animations.play('fly'); 
        //
        
        this.overLabel = game.add.text(game.width/2, -50, 'GAME OVER', { font: '30px Arial', fill: '#ffffff' });
        game.add.tween(this.overLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
        this.overLabel.anchor.setTo(0.5, 0.5);

        this.scoreLabel = game.add.text(game.width/2, game.height/2, 'score: '+game.global.score, { font: '20px Arial', fill: '#ffffff' });
        this.hint = game.add.text(game.width/2, game.height-50, 'end(esc key) or again(enter key)', { font: '20px Arial', fill: '#ffffff' });
        this.scoreLabel.anchor.setTo(0.5, 0.5);
        this.hint.anchor.setTo(0.5,0.5);
        game.add.tween(this.hint).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        var escKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

        
        enterKey.onDown.add(this.again, this);
        escKey.onDown.add(this.end, this);
    },
    again: function()
    {
        game.state.start('play');
    },
    end: function()
    {
        game.state.start('menu');
    }
};