var loadState = {
  preload: function () {
    // Add a 'loading...' label on the screen
    game.load.image('menuBg', 'assets/space1.jpg');
    this.bg = this.game.add.tileSprite(0, 0, window.innerWidth, window.innerHeight, 'loadbg');
    var loadingLabel = game.add.text(game.width/2, 150,
    'loading...', { font: '30px Arial', fill: '#ffffff' });
    loadingLabel.anchor.setTo(0.5, 0.5);
    // Display the progress bar
    var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
    progressBar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(progressBar);
    // Load all game assets
    game.load.spritesheet('player', 'assets/player.png', 20, 20);
    game.load.spritesheet('wing', 'assets/wing_spritesheet.png', 70, 41);
    game.load.image('alpaca','assets/alpaca1.png');
    game.load.spritesheet('player1', 'assets/llama_walk.png', 64, 64);
    ////bullets
    game.load.image('playerBullet', 'assets/bullet2.png');
    game.load.image('enemyBullet', 'assets/pupu.png');
    game.load.image('tissue', 'assets/tissue.png');

    ////enemies
    game.load.image('enemy', 'assets/toilet.png');
    game.load.image('enemy2', 'assets/enemy2.png');
    game.load.image('coin', 'assets/coin.png');
    game.load.image('wallV', 'assets/wallVertical.png');
    game.load.image('bar', 'assets/bar.png');
    game.load.image('wallH', 'assets/wallHorizontal.png');
    game.load.image('sound', 'assets/sound.png');
    // Load a new asset that we will use in the menu state
    game.load.image('background', 'assets/background.jpg');
    /////sound effect
    game.load.audio('bgm','assets/bgm.wav');
    game.load.audio('playerShoot','assets/player_shoot.mp3');
    game.load.audio('enemyShoot','assets/enemy_shoot.mp3');
    game.load.audio('dieSound', 'assets/die.mp3');
    //
    //game.load.image('pixel', 'assets/pixel.png');
    game.load.image('pixel', 'assets/leaf.png');
    game.load.image('enemy_pixel', 'assets/pupu_pixel.png');

  },
  create: function() {
    // Go to the menu state
    game.state.start('menu');
  }
}; 