var alreadyShoot=0;
var livingEnemies1 = [];
var livingenemies2 = [];
var special = 0, enemy_special=0;

var init=1;
var scoreRef = firebase.database().ref('scoreBoard');
var playState = {
  create: function() {
    game.physics.startSystem(Phaser.Physics.ARCADE);
    
    this.getFirebase();
    ///bg
    this.bg = this.game.add.tileSprite(0, 0, window.innerWidth, window.innerHeight, "background");
    // PLAYER
    this.player = game.add.sprite(game.width/2-10, game.height*5/6, 'player1');
    this.alive=1;
    // this.player.('outOfBoundsKill', true);
    // this.helpers.setAll('checkWorldBounds', true); 
    //this.player.anchor.setTo(0.3,0.3);
    game.physics.arcade.enable(this.player);

    this.player.animations.add('rightwalk', [12, 13, 14, 15], 8, true);
    this.player.animations.add('leftwalk', [4, 5, 6, 7], 8, true);

    // WALL
     this.walls = game.add.group(); 
     this.walls.enableBody = true;
    
     game.add.sprite(0, 0, 'wallV', 0, this.walls); 
     game.add.sprite(game.width-20, 0, 'wallV', 0, this.walls);
     


     this.walls.setAll('body.immovable', true);

    // INPUT
    this.cursor = game.input.keyboard.createCursorKeys();
    this.shoot = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    this.special = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.pause = game.input.keyboard.addKey(Phaser.Keyboard.P);
    this.level1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
    this.level2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
    this.level3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
    this.level4 = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
    this.die = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

    this.pause.onDown.add(this.pauseSenser, self);
    // sound
    this.bgm = game.add.audio('bgm');
    this.playerShootsnd = game.add.audio('playerShoot');
    this.enemyShootsnd = game.add.audio('enemyShoot');
    this.dieSound = game.add.audio('dieSound');
    this.bgm.loopFull();
    //volume ctrl
    //add empty sprite for input detection
    this.soundbar = game.add.sprite(game.width-60, 30,'bar');
    this.soundbar.anchor.setTo(0, 0);
    this.soundctrl = game.add.sprite(game.width-60, 75,'sound');
    this.soundctrl.anchor.setTo(0, 0);
    this.soundctrl.scale.setTo(1);
    this.soundctrl.inputEnabled = true;
    this.soundctrl.input.enableDrag(false,false,false,255,this.soundbar);
    this.soundctrl.input.allowHorizontalDrag = false;
    
    // COIN
    // this.coin = game.add.sprite(60, 140, 'coin');
    // game.physics.arcade.enable(this.coin);
    // this.coin.anchor.setTo(0.5, 0.5);

    //level
    game.global.level = 1;
    this.levelLabel = game.add.text(game.width-140, 30, 'level: 1', { font: '18px Arial', fill: '#ffffff' });
    //ultimate skill
    this.skill= 0;
    this.skillcnt=0;
    this.skillLabel = game.add.text(game.width-140, 60, 'skill: 0%', { font: '18px Arial', fill: '#ffffff' });
    //
    this.tissueCnt= 0;
    this.tissueCntLabel = game.add.text(game.width-140, 90, 'tissue: 0', { font: '18px Arial', fill: '#ffffff' });
    // SCORE
    game.global.score = 0;
    this.scoreLabel = game.add.text(30, 30, 'score: 0', { font: '18px Arial', fill: '#ffffff' });
    //blood
    game.global.blood = 5;
    this.bloodLabel = game.add.text(30, 60, 'blood: 5', { font: '18px Arial', fill: '#ffffff' });
    //name
    this.nameLabel = game.add.text(30, 90, 'name: '+ game.global.name, { font: '18px Arial', fill: '#ffffff' });
    // ENEMY
    this.enemies1 = game.add.group();
    game.physics.arcade.enable(this.enemies1);
    this.enemies1.enableBody = true;
    // ENEMY2
    this.enemies2 = game.add.group();
    game.physics.arcade.enable(this.enemies2);
    this.enemies2.enableBody = true;
    // Create 10 enemies in the group.
    this.enemies1.createMultiple(10, 'enemy');
    this.enemies2.createMultiple(10, 'enemy2');
    game.time.events.loop(2000, this.addEnemy, this);
    game.time.events.loop(2500, this.addEnemy2, this);
    game.time.events.loop(1500, this.enemy1Shoot, this);
    game.time.events.loop(1000, this.enemy2Shoot, this);
    
    //player.bullet
    this.player.bullets = game.add.group();
    this.player.bullets.enableBody = true;
    this.player.bullets.createMultiple(30, 'playerBullet');
    //helper
    this.helpers= game.add.group();
    this.helpers.enableBody = true;
    this.helpers.enableBody = true; 
    this.helpers.physicsBodyType = Phaser.Physics.ARCADE; 
    this.helpers.createMultiple(30, 'tissue');
    this.helpers.setAll('anchor.x', 0.5);
    this.helpers.setAll('anchor.y', 0);
    this.helpers.setAll('outOfBoundsKill', true);
    this.helpers.setAll('checkWorldBounds', true);    
    //enemies bullet
    this.enemyBullets= game.add.group();
    this.enemyBullets.enableBody = true;
    this.enemyBullets.enableBody = true; 
    this.enemyBullets.physicsBodyType = Phaser.Physics.ARCADE; 
    this.enemyBullets.createMultiple(30, 'enemyBullet');
    this.enemyBullets.setAll('anchor.x', 0.5);
    this.enemyBullets.setAll('anchor.y', 0);
    this.enemyBullets.setAll('outOfBoundsKill', true);
    this.enemyBullets.setAll('checkWorldBounds', true);

    // PARTICLE
    this.emitter = game.add.emitter(0, 0, 30);
    this.emitter.makeParticles('pixel');
    this.emitter.setYSpeed(-150, 150);
    this.emitter.setXSpeed(-150, 150);
    this.emitter.setScale(2, 0, 2, 0, 800);
    this.emitter.gravity = 0;
    //enemy pixel
    this.enemy_emitter = game.add.emitter(0, 0, 15);
    this.enemy_emitter.makeParticles('enemy_pixel');
    this.enemy_emitter.setYSpeed(-150, 150);
    this.enemy_emitter.setXSpeed(-150, 150);
    this.enemy_emitter.setScale(2, 0, 2, 0, 800);
    this.enemy_emitter.gravity = 0;
  },
  update: function() {
    //scroll bg
    this.bg.tilePosition.y += 2;
    // COLLISION
     game.physics.arcade.collide(this.enemies1, this.walls);
     game.physics.arcade.collide(this.enemies2, this.walls);
     game.physics.arcade.collide(this.player, this.walls);
     game.physics.arcade.collide(this.enemyBullets, this.walls);
     game.physics.arcade.collide(this.helpers, this.walls);
     game.physics.arcade.collide(this.player.bullets, this.walls);

     //game.physics.arcade.collide(this.enemies, this.player);
    // game.physics.arcade.collide(this.walls, this.walls);
    game.physics.arcade.overlap(this.player, this.enemyBullets, this.playerDie, null, this);
    game.physics.arcade.overlap(this.player.bullets, this.enemyBullets, this.bulletDie, null, this);
    game.physics.arcade.overlap(this.player.bullets, this.helpers, this.playerHarm, null, this);
    game.physics.arcade.overlap(this.player, this.helpers, this.playerHelp, null, this);
    game.physics.arcade.overlap(this.enemies1, this.player.bullets, this.enemyDie, null, this);
    game.physics.arcade.overlap(this.enemies2, this.player.bullets, this.enemyDie, null, this);
    game.physics.arcade.overlap(this.player,this.enemies1, this.playerDie, null, this);
    // game.physics.arcade.overlap(this.player, this.coin, this.takeCoin, null, this);

    this.movePlayer();
    this.playerAttack();
    this.setVolume();
    this.setLevel();
    this.setText();
    
    //this.pauseSenser();
    //this.enemiesAttack();
    if (!this.player.inWorld) { this.playerDie();}
    if(this.die.isDown)
    {
        this.updateFirebase();
        this.alive=0;
          this.player.kill();
          this.bgm.stop();
          // PARTICLE SYSTEM
          this.emitter.x = this.player.x;
          this.emitter.y = this.player.y;
          this.emitter.start(true, 800, null, 15);
          this.dieSound.play();

          game.time.events.add(1000, function() {
            game.state.start('game_over');
          }, this);
    }


    
    
  },
  setText: function()
  {
    this.skillLabel.text = "skill: " + this.skill+"%";
    this.bloodLabel.text = 'blood: ' + game.global.blood;
    this.scoreLabel.text = "score: " + game.global.score;
    this.levelLabel.text = "level: " + game.global.level;
    this.tissueCntLabel.text = "tissue: " + this.tissueCnt;
  },
  getFirebase: function()
  {
    var user_name = "mike";
    var score = 1;
    scoreRef.on('child_added',snap=>{
       var score = snap.val().score;
      var name = snap.val().user;
      
    });
    
  },
  updateFirebase: function()
  {
      
    console.log(game.global.score);
    var user = game.global.name;
    var pushscore = game.global.score + this.tissueCnt*6 + game.global.blood*7 + game.global.level*3;
    // alert(pushscore);
    var scoreData = {
      user: user,
      score: pushscore
    };
    if(this.alive)
      scoreRef.push(scoreData);
    
  },
  pauseSenser:function()
  {
    game.paused = !game.paused;
  },
  setLevel: function()
  {
    if(game.global.score==20)
    {
      game.global.level = 2;
      game.global.score = 20;
    }
    else if(game.global.score==50)
    {
      game.global.level = 3;
      game.global.score = 50;
    }
    else if(game.global.score==70)
    {
      game.global.level = 4;
      game.global.score = 70;
      console.log('p');
    }

    if(this.level1.isDown)
    {
      game.global.score = 0;
    }
    else if(this.level2.isDown)
    {
      game.global.score = 20;
    } 
    else if(this.level3.isDown)
    {
      game.global.score = 50;
    }
    else if(this.level4.isDown)
    {
      game.global.score = 70;
    }

  },
  setVolume: function()
  {
    
    var percentage = (this.soundctrl.y-this.soundbar.y)/85.0;
    this.bgm.volume = percentage;
    this.playerShootsnd.volume = percentage;
    this.enemyShootsnd.volume = percentage;
    this.dieSound.volume = percentage;
    
  },
  playerAttack: function(){
    
    if(this.shoot.isDown&&!alreadyShoot)
    {
      if(this.special.isDown&&this.skill==100)
      {
        special=1;
        if(this.skillcnt<4)
        {
          this.skillcnt++;
        }
        else
        {
          this.skillcnt=0;
          this.skill=0;
        }
      }
      this.addBullets();
      alreadyShoot=1;
    }
    else if(this.shoot.isUp)
    {
      alreadyShoot=0;
      special=0;
    }

  },
  movePlayer: function() {

    // LEFT RIGHT
    if (this.cursor.left.isDown) {
      this.player.body.velocity.x = -200;
      this.player.animations.play('leftwalk'); 
    }
    else if (this.cursor.right.isDown) {
      this.player.body.velocity.x = 200;
      this.player.animations.play('rightwalk'); 
    }
    else {
      this.player.body.velocity.x = 0;
      this.player.animations.stop();
      this.player.frame = 0; // Set frame (0 : stand)
    } 
    
  },
  addBullets: function() {
    var speedx = [0, 150, -150];
    var n=(special==1)?3:1;
    for(var i=0;i<n;i++)
    {
      var bullet = this.player.bullets.getFirstDead();
      if (!bullet) return;
      bullet.anchor.setTo(0.5, 1);
      bullet.reset(this.player.x+30, this.player.y);
      bullet.body.gravity.y = 0;
      //enemy.body.velocity.y = 0;
      bullet.body.velocity.x = speedx[i];
      bullet.body.velocity.y = -300;
      bullet.body.bounce.x = 1;
      bullet.checkWorldBounds = true;
      bullet.outOfBoundsKill = true;
    }
    this.playerShootsnd.play();
  },
  addEnemy: function() {
    var enemy = this.enemies1.getFirstDead();    
    if (!enemy) return;
    enemy.anchor.setTo(0.5, 1);
    enemy.reset(game.width/2, 0);
    enemy.body.gravity.y = 10;
    //enemy.body.velocity.y = 0;
    enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
    enemy.body.bounce.x = 1;
    enemy.checkWorldBounds = true;
    enemy.outOfBoundsKill = true;

  },
  addEnemy2: function() {
    if(game.global.level>1)
    {
      var enemy = this.enemies2.getFirstDead();    
      if (!enemy) return;
      enemy.anchor.setTo(0.5, 0);
      // enemy.anchor.setTo(0.5, 1);
      enemy.reset(game.width/2, 0);
      enemy.body.gravity.y = 10;
      //enemy.body.velocity.y = 0;
      enemy.body.velocity.x = 100 * game.rnd.pick([-1, 1]);
      enemy.body.bounce.x = 1;
      enemy.checkWorldBounds = true;
      enemy.outOfBoundsKill = true;
    }

  },
  enemy1Shoot:function()
  {
    livingEnemies1.length = 0; 
    this.enemies1.forEachAlive(function(enemy){
        if(enemy.body.y < game.height)
          livingEnemies1.push(enemy);
        //console.log(game.width);
    });
    
    var n = (game.global.level==4)?3:1;
    var speedx = [0, 150, -150];
    var random = this.rnd.integerInRange(1, 4);
    var enemyBullet;
    // console.log(random);
    enemyBullet = this.enemyBullets.getFirstExists(false); 
    
    if(enemyBullet && livingEnemies1.length > 0) {
      //enemyShotSound.play();
      random = this.rnd.integerInRange(0, livingEnemies1.length - 1);
      var shooter = livingEnemies1[random];
      for(var i=0;i<n;i++)
      {
        enemyBullet.reset(shooter.body.x, shooter.body.y + 30);
        //enemyBulletTime = this.time.now + 500;
        this.enemyShootsnd.play();
        enemyBullet.body.velocity.y = (n==1)?300:200;
        enemyBullet.body.velocity.x = speedx[i];
        enemyBullet = this.enemyBullets.getFirstExists(false);
        
      }
  }
  },
  enemy2Shoot: function() {
    livingenemies2.length = 0; 
    this.enemies2.forEachAlive(function(enemy){
        if(enemy.body.y < game.height)
          livingenemies2.push(enemy);
        //console.log(game.width);
    });
    
    var n = (game.global.level==4)?3:1;
    var speedx = [0, 150, -150];
    var random;
    var enemyBullet;
    random = this.rnd.integerInRange(1, 2);
    // console.log(random);
    if(random==3||game.global.level>2)
    {
      enemyBullet = this.helpers.getFirstExists(false); 
    }
    else
    {
      enemyBullet = this.enemyBullets.getFirstExists(false); 
    }
    ///start here
    if(enemyBullet && livingenemies2.length > 0) {
        //enemyShotSound.play();
        random = this.rnd.integerInRange(0, livingenemies2.length - 1);
        var shooter = livingenemies2[random];
        for(var i=0;i<n;i++)
        {
          enemyBullet.reset(shooter.body.x, shooter.body.y + 30);
          //enemyBulletTime = this.time.now + 500;
          this.enemyShootsnd.play();
          enemyBullet.body.velocity.y = (n==1)?300:200;
          enemyBullet.body.velocity.x = speedx[i];

          random = this.rnd.integerInRange(1, 2);
          // console.log(random);
          if(random==3&&n==3)
          {
            enemyBullet = this.helpers.getFirstExists(false); 
          }
          else
          {
            enemyBullet = this.enemyBullets.getFirstExists(false); 
          }
          
        }
    }
  },
  bulletDie:function(bullet, enemyBullet)
  {
    // SCORE
    game.global.score += 5;
    this.skill=(this.skill==100)?100:this.skill+20;
    this.scoreLabel.text = 'score: ' + game.global.score;
    // PARTICLE SYSTEM
    this.enemy_emitter.x = enemyBullet.x;
    this.enemy_emitter.y = enemyBullet.y;
    this.enemy_emitter.start(true, 800, null, 15);
    
    bullet.kill();
    enemyBullet.kill();

  },
  enemyDie: function(bullet, enemy) {
    bullet.kill();
    if(enemy.y>30)
    {
      //console.log(enemy.y);
      // SCORE
      game.global.score += 5;
      this.skill=(this.skill==100)?100:this.skill+20;
      this.scoreLabel.text = 'score: ' + game.global.score;
      // PARTICLE SYSTEM
      this.enemy_emitter.x = enemy.x;
      this.enemy_emitter.y = enemy.y;
      this.enemy_emitter.start(true, 800, null, 15);
      enemy.kill();
    }

    // CAMERA EFFECT
    //game.camera.flash(0xffffff, 300);
    //game.camera.shake(0.02, 300);

    // game.time.events.add(1000, function() {
    //   game.state.start('menu');
    // }, this);
  },
  playerHelp: function(player, helper)
  {
    this.skill = 100;
    this.skillcnt=0;
    game.global.blood++;
    this.tissueCnt++;
    
    helper.kill();
  },
  playerHarm: function(bullet, helper)
  {
    bullet.kill();
    game.global.blood--;
    helper.kill();
    // player PARTICLE SYSTEM
    this.emitter.x = this.player.x;
    this.emitter.y = this.player.y;
    this.emitter.start(true, 800, null, 50);        
    game.camera.shake(0.02, 300);

  },
  playerDie: function(player, bullet) {
    if(!this.player.inWorld)
      this.player.kill();
    else if(Math.abs(this.player.x-bullet.x)<15||Math.abs(this.player.y-bullet.y)<10)
    {
      console.log("x:"+Math.abs(this.player.x-bullet.x));
      console.log("y:"+Math.abs(this.player.y-bullet.y));
      game.global.blood--;
      this.skill=0;
      // PARTICLE SYSTEM
      this.emitter.x = this.player.x;
      this.emitter.y = this.player.y;
      this.emitter.start(true, 800, null, 50);        
      game.camera.shake(0.02, 300);
      bullet.kill();

      if(game.global.blood==0)
      {
        this.updateFirebase();
          this.player.kill();
          this.bgm.stop();
          // PARTICLE SYSTEM
          this.emitter.x = this.player.x;
          this.emitter.y = this.player.y;
          this.emitter.start(true, 800, null, 15);
          this.dieSound.play();

          game.time.events.add(1000, function() {
            game.state.start('game_over');
          }, this);
      }

    }
    
  }
};