var menuState = {
  create: function() {
    // Add a background image
    //game.add.image(0, 0, 'background');
    // Display the name of the game
    this.bg = this.game.add.tileSprite(0, 0, window.innerWidth, window.innerHeight, "menuBg");
    var nameLabel = game.add.text(game.width/4, -50, 'very simple game', { font: '30px Arial', fill: '#ffffff' });
    nameLabel.anchor.setTo(0.5, 0.5);
    this.addKeyboard();

    this.scores = [];
    this.names = [];
    this.already = [];
    game.global.name = "";
    for(var i=0;i<27;i++)
    {
      this.already.push(0);
    }
    this.length = 0;
    this.init=1;
    this.getFirebase();
    game.add.tween(nameLabel).to({y: 80}, 1000).easing(Phaser.Easing.Bounce.Out).start();
    // Show the score at the center of the screen
    this.hint = "(type your name here)";
    this.scoreLabel = [];
    for(var i=0;i<5;i++)
    {
      this.scoreLabel[i] = game.add.text(game.width/6, 120+20*i,
      (i+1)+'. '+"loading...", { font: '20px Arial', fill: '#ffffff' });
      this.scoreLabel[i].anchor.setTo(0, 0);
    }
    this.nameLabel = game.add.text(game.width/2+100, game.height-90,
      'your name:'+this.hint , { font: '20px Arial', fill: '#ffffff' });
    this.nameLabel.anchor.setTo(0.5, 0.5);
    
    // Explain how to start the game
    var startLabel = game.add.text(game.width/2+100, game.height-50, 'press the enter key to start', { font: '20px Arial', fill: '#ffffff' });
    startLabel.anchor.setTo(0.5, 0.5);
    game.add.tween(startLabel).to({angle: -2}, 500).to({angle: 2}, 1000).to({angle: 0}, 500).loop().start();
    // Create a new Phaser keyboard variable: the up arrow key
    // When pressed, call the 'start'
    var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
    enterKey.onDown.add(this.start, this);
  },
  start: function() {
    // Start the actual game
    if(game.global.name!="")
      game.state.start('play');
  },
  update: function()
  {
    this.levelBoard();
    this.getName();
    console.log(game.global.name);
  },
  getName: function()
  {
    //console.log(this.alreadyDown);
    if(game.global.name!='')
      this.nameLabel.text = "your name:"+ game.global.name;
    else this.nameLabel.text = "your name:"+ this.hint;
    if(this.A.isDown&&!this.already[0])
    {
      game.global.name+="a";
      this.already[0]=1;
      //console.log('a');
    }
    else if(this.A.isUp)
    {
      this.already[0]=0;
    }
    
    
    if(this.B.isDown&&!this.already[1])
    {
      game.global.name+="b";
      this.already[1]=1;
    }
    else if(this.B.isUp)
    {
      this.already[1]=0;
    }
    

    if(this.C.isDown&&!this.already[2])
    {
      game.global.name+="c";
      this.already[2]=1;

    }
    else if(this.C.isUp)
    {
      this.already[2]=0;
    }
    if(this.D.isDown&&!this.already[3])
    {
      game.global.name+="d";
      this.already[3]=1;

    }
    else if(this.D.isUp)
    {
      this.already[3]=0;
    }
    if(this.E.isDown&&!this.already[4])
    {
      game.global.name+="e";
      this.already[4]=1;

    }
    else if(this.E.isUp)
    {
      this.already[4]=0;
    }
    if(this.F.isDown&&!this.already[5])
    {
      game.global.name+="f";
      this.already[5]=1;

    }
    else if(this.F.isUp)
    {
      this.already[5]=0;
    }
    if(this.G.isDown&&!this.already[6])
    {
      game.global.name+="g";
      this.already[6]=1;

    }
    else if(this.G.isUp)
    {
      this.already[6]=0;
    }
    if(this.H.isDown&&!this.already[7])
    {
      game.global.name+="h";
      this.already[7]=1;

    }
    else if(this.H.isUp)
    {
      this.already[7]=0;
    }
    if(this.I.isDown&&!this.already[8])
    {
      game.global.name+="i";
      this.already[8]=1;

    }
    else if(this.I.isUp)
    {
      this.already[8]=0;
    }
    if(this.J.isDown&&!this.already[9])
    {
      game.global.name+="j";
      this.already[9]=1;

    }
    else if(this.J.isUp)
    {
      this.already[9]=0;
    }
    if(this.K.isDown&&!this.already[10])
    {
      game.global.name+="k";
      this.already[10]=1;

    }
    else if(this.K.isUp)
    {
      this.already[10]=0;
    }
    if(this.L.isDown&&!this.already[11])
    {
      game.global.name+="l";
      this.already[11]=1;

    }
    else if(this.L.isUp)
    {
      this.already[11]=0;
    }
    if(this.M.isDown&&!this.already[12])
    {
      game.global.name+="m";
      this.already[12]=1;

    }
    else if(this.M.isUp)
    {
      this.already[12]=0;
    }
    if(this.N.isDown&&!this.already[13])
    {
      game.global.name+="n";
      this.already[13]=1;

    }
    else if(this.N.isUp)
    {
      this.already[13]=0;
    }
    if(this.O.isDown&&!this.already[14])
    {
      game.global.name+="o";
      this.already[14]=1;

    }
    else if(this.O.isUp)
    {
      this.already[14]=0;
    }
    if(this.P.isDown&&!this.already[15])
    {
      game.global.name+="p";
      this.already[15]=1;

    }
    else if(this.P.isUp)
    {
      this.already[15]=0;
    }
    if(this.Q.isDown&&!this.already[16])
    {
      game.global.name+="q";
      this.already[16]=1;

    }
    else if(this.Q.isUp)
    {
      this.already[16]=0;
    }
    if(this.R.isDown&&!this.already[17])
    {
      game.global.name+="r";
      this.already[17]=1;

    }
    else if(this.R.isUp)
    {
      this.already[17]=0;
    }
    if(this.S.isDown&&!this.already[18])
    {
      game.global.name+="s";
      this.already[18]=1;

    }
    else if(this.S.isUp)
    {
      this.already[18]=0;
    }
    if(this.T.isDown&&!this.already[19])
    {
      game.global.name+="t";
      this.already[19]=1;

    }
    else if(this.T.isUp)
    {
      this.already[19]=0;
    }
    if(this.U.isDown&&!this.already[20])
    {
      game.global.name+="u";
      this.already[20]=1;
    }
    else if(this.U.isUp)
    {
      this.already[20]=0;
    }
    if(this.V.isDown&&!this.already[21])
    {
      game.global.name+="v";
      this.already[21]=1;

    }
    else if(this.V.isUp)
    {
      this.already[21]=0;
    }
    if(this.W.isDown&&!this.already[22])
    {
      game.global.name+="w";
      this.already[22]=1;

    }
    else if(this.W.isUp)
    {
      this.already[22]=0;
    }
    if(this.X.isDown&&!this.already[23])
    {
      game.global.name+="x";
      this.already[23]=1;

    }
    else if(this.X.isUp)
    {
      this.already[23]=0;
    }
    if(this.Y.isDown&&!this.already[24])
    {
      game.global.name+="y";
      this.already[24]=1;

    }
    else if(this.Y.isUp)
    {
      this.already[24]=0;
    }
    if(this.Z.isDown&&!this.already[25])
    {
      game.global.name+="z";
      this.already[25]=1;

    }
    else if(this.Z.isUp)
    {
      this.already[25]=0;
    }
    if(this.delete.isDown&&!this.already[26])
    {
      //game.global.name.pop();
      var len = game.global.name.length;
      if(len!=1)
        game.global.name = game.global.name.substring(0,len-1);
      else game.global.name="";
      console.log(game.global.name.length);
      this.already[26]=1;
    }
    else if(this.delete.isUp)
    {
      this.already[26]=0;
    }

    
  },
  getFirebase: function()
  {
    var scoreRef = firebase.database().ref('scoreBoard'); 
    
    scoreRef.orderByChild("score").on('child_added',snap=>{
      //console.log("p");
      var score = snap.val().score;
      var name = snap.val().user;
      this.scores.push(score);
      this.names.push(name);
      //this.sort();
      
    });
    //console.log(scores.length);
        
  },
  levelBoard: function()
  {
    if(this.init&&this.scores.length!=0)
    {
      for(var i=0;i<this.scores.length&&i<5;i++)
      {
        console.log(this.scores[this.scores.length-i-1]);
        this.scoreLabel[i].text = (i+1)+". " + this.names[this.scores.length-i-1]+" "+this.scores[this.scores.length-i-1];
      }
            
      this.init=0;
    }
    else if(this.scores.length>this.length)
    {
      for(var i=0;i<this.scores.length&&i<5;i++)
      {
        console.log(this.scores[this.scores.length-i-1]);
        this.scoreLabel[i].text = (i+1)+". "+this.names[this.scores.length-i-1]+" "+this.scores[this.scores.length-i-1];
      }
    }
    this.length = this.scores.length;
  },
  sort: function()
  {
    for(var i=0;i<this.scores.length;i++)
    {
      for(var j=i+1;j<this.scores.length;j++)
      {
        if(this.scores[i]>this.scores[j])
        {
          var temp = this.scores[i];
          var tempname = this.names[i];

          this.scores[i] = this.scores[j];
          this.scores[j] = temp;
          
          this.names[i] = this.names[j];
          this.names[j] = tempname;
        }
      }
    }
  },
  addKeyboard: function()
  {
    this.A = game.input.keyboard.addKey(Phaser.Keyboard.A);
    this.B = game.input.keyboard.addKey(Phaser.Keyboard.B);
    this.C = game.input.keyboard.addKey(Phaser.Keyboard.C);
    this.D = game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.E = game.input.keyboard.addKey(Phaser.Keyboard.E);
    this.F = game.input.keyboard.addKey(Phaser.Keyboard.F);
    this.G = game.input.keyboard.addKey(Phaser.Keyboard.G);
    this.H = game.input.keyboard.addKey(Phaser.Keyboard.H);
    this.I = game.input.keyboard.addKey(Phaser.Keyboard.I);
    this.J = game.input.keyboard.addKey(Phaser.Keyboard.J);
    this.K = game.input.keyboard.addKey(Phaser.Keyboard.K);
    this.L = game.input.keyboard.addKey(Phaser.Keyboard.L);
    this.M = game.input.keyboard.addKey(Phaser.Keyboard.M);
    this.N = game.input.keyboard.addKey(Phaser.Keyboard.N);
    this.O = game.input.keyboard.addKey(Phaser.Keyboard.O);
    this.P = game.input.keyboard.addKey(Phaser.Keyboard.P);
    this.Q = game.input.keyboard.addKey(Phaser.Keyboard.Q);
    this.R = game.input.keyboard.addKey(Phaser.Keyboard.R);
    this.S = game.input.keyboard.addKey(Phaser.Keyboard.S);
    this.T = game.input.keyboard.addKey(Phaser.Keyboard.T);
    this.U = game.input.keyboard.addKey(Phaser.Keyboard.U);
    this.V = game.input.keyboard.addKey(Phaser.Keyboard.V);
    this.W = game.input.keyboard.addKey(Phaser.Keyboard.W);
    this.X = game.input.keyboard.addKey(Phaser.Keyboard.X);
    this.Y = game.input.keyboard.addKey(Phaser.Keyboard.Y);
    this.Z = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    this.delete = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);  

  }

}; 